Name:      igwn-backports-config
Version:   8
Release:   8%{?dist}
Summary:   Yum/DNF configuration for IGWN Backports Repository

License:   GPLv3+
BuildArch: noarch
Requires:  redhat-release >= %{version}
Url: https://software.igwn.org/lscsoft/rocky/%{version}/production/

Source0:   igwn-backports.repo
Source1:   COPYING
Source2:   README-igwn-backports.md

Provides:  lscsoft-backports-config = %{version}-%{release}
Obsoletes: lscsoft-backports-config < 8-3

%description
Yum/DNF configuration for IGWN Backports Repository on Rocky Linux %{version}

%prep
%setup -q -c -T
install -pm 644 %{SOURCE1} .
install -pm 644 %{SOURCE2} .

%build

%install
install -dm 755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README-igwn-backports.md
%license COPYING
%config(noreplace) %{_sysconfdir}/yum.repos.d/igwn-backports.repo

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Fri Feb 21 2025 Adam Mercer <adam.mercer@ligo.org> 8-8
- Fix typo in changelog

* Fri Feb 21 2025 Adam Mercer <adam.mercer@ligo.org> 8-7
- Use https URLs

* Mon Feb 01 2023 Adam Mercer <adam.mercer@ligo.org> 8-6
- add source repository

* Tue Jan 03 2023 Adam Mercer <adam.mercer@ligo.org> 8-5
- fix repository description

* Tue Dec 27 2022 Adam Mercer <adam.mercer@ligo.org> 8-3
- rename to igwn-backports-config

* Fri Jul 30 2021 Adam Mercer <adam.mercer@ligo.org> 8-2
- update URL for Rocky Linux

* Thu Jan 07 2021 Adam Mercer <adam.mercer@ligo.org> 8-1
- initial release
