# IGWN Backports Yum/DNF Repository

The IGWN Backports Yum/DNF Repository contains RPMs for production-ready
backported software produced by IGWN member groups.

See <https://computing.docs.ligo.org/guide/software/> for details on
IGWN Software practices.